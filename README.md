# t5-large-ui

To install dependencies:

```bash
bun install
```

To run:

```bash
bun run start
```
