from transformers import AutoTokenizer, AutoModelForSeq2SeqLM
import sys

tokenizer = AutoTokenizer.from_pretrained("google/flan-t5-large")
model = AutoModelForSeq2SeqLM.from_pretrained("google/flan-t5-large")

input_text = sys.argv[1]
input_ids = tokenizer(input_text, return_tensors="pt").input_ids

outputs = model.generate(input_ids, max_length=200)
print(tokenizer.decode(outputs[0]))
