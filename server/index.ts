import path from "node:path";

const server = Bun.serve({
    port: 8080,
    async fetch(request, server) {
        const url = new URL(request.url);

        if (url.pathname === "/api/t5" && request.method === "POST") {
            // run our python code
            const { stdout } = Bun.spawn(
                [
                    "python3",
                    path.resolve(import.meta.dir, "index.py"),
                    await request.text(),
                ],
                {
                    stdout: "pipe",
                }
            );

            // return result
            const res = await new Response(stdout).text();
            return new Response(res);
        } else {
            return new Response(
                Bun.file(
                    path.resolve(import.meta.dir, "../", "client/index.html")
                )
            ); // return html
        }
    },
});

console.log(
    "\x1b[92mServer Started:\x1b[0m",
    server.port,
    `(http://localhost:${server.port})`
);
